<h1>Grupo 3 Projeto 2 - Servidor Chat IRC Web utlizando WebSocket (Socket.io) </h1>

**Universidade Federal de Goiás (UFG)** </br>

Disciplina: Aplicações Distribuídas(AD) </br>

Professor: Marcelo Akira </br>

Colaboradores:
<ul>
<li>Matheus França - 201517342 (Lider/Desenvolvedor)</li>
<li>Laura Caroline - 137101 (Desenvolvedora)</li>
<li>Antonio Tavares - 201614873(Documentador)</li>
<li>Gerson Rosolim - 20130714(Desenvolvedor)</li>
<li>Matheus Santos Carvalho - 1506648 (Desenvolvedor)</li>
<li>Rogerio Amorim Marinho Junior - 1517351(Desenvolvedor)</li> 
</ul>
<hr>
<h2>Como instalar:</h2> </br>
baixe as bibliotecas utilizadas no projeto: </br>
<pre> npm install</pre></br>
Rode o servidor 
<pre> node app.js </pre> </br>

acesse o servidor em : [http://localhost:3000](http://localhost:3000)
