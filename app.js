var express = require('express'); // módulo express
var app = express(); // objeto express
var server = require('http').Server(app);
var socketio = require('socket.io')(server);

var bodyParser = require('body-parser'); // processa corpo de requests
var cookieParser = require('cookie-parser'); // processa cookies
var irc = require('irc');
var comandosImplementados = require('./public/js/commandsMap').commandsMap;

//Listeneres add
var addGeneralListeners = require('./public/js/generalListeners').AddGeneralListeners;
var addListenersGroup1 = require('./public/js/commandsGroup1/listeners').AddlistenersGroup1;
var addListenersGroup2 = require('./public/js/commandsGroup2/listeners').AddlistenersGroup2;
var addListenersGroup3 = require('./public/js/commandsGroup3/listeners').AddlistenersGroup3;
var addListenersGroup4 = require('./public/js/commandsGroup4/listeners').AddlistenersGroup4;

//Commands map
const commandsMap = require("./public/js/commandsMap").commandsMap;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));

var path = require('path'); // módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;

function proxy(id, servidor, nick, canal, socket) {
    var ws = socket;

    var irc_client = new irc.Client(
        servidor,
        nick, { channels: [canal], });

    irc_client = addGeneralListeners(irc_client, ws, canal);
    irc_client = addListenersGroup1(irc_client, ws);
    irc_client = addListenersGroup2(irc_client, ws);
    irc_client = addListenersGroup3(irc_client, ws);
    irc_client = addListenersGroup4(irc_client, ws);

    proxies[id] = { "ws": socket, "irc_client": irc_client };

    return proxies[id];
}

app.get('/', function(req, res) {

    if (req.cookies.servidor && req.cookies.nick && req.cookies.canais) {
        proxy_id++;
        socketio.on("connection", function(socket) {
            var p = proxy(proxy_id,
                req.cookies.servidor,
                req.cookies.nick,
                req.cookies.canais,
                socket);
            socket.on("message", function(message) {
                proxies[proxy_id].irc_client.say(req.cookies.canais, message);
            })
        });
        res.cookie('id', proxy_id);
        res.sendFile(path.join(__dirname, '/public/chatpage.html'));
    } else {
        res.sendFile(path.join(__dirname, '/public/login.html'));
    }
});

app.post('/processar_mensagem', function(req, res) {

    var irc_client = proxies[req.cookies.id].irc_client;
    var ws = proxies[req.cookies.id].ws;
    var proxy = proxies[req.cookies.id]
    var message = req.body.msg.trim().replace("/", "");
    var args = message.split(" ");
    var command = args[0].toUpperCase();

    if (Object.keys(commandsMap).includes(command)) {
        commandsMap[command](irc_client, ws, proxy, args);
        res.end();
    } else {
        res.send(JSON.stringify({ error: "Comando não encontrado." }));
    }

});

app.get('/comandos', function(req, res) {
    res.send(JSON.stringify({ comandos: Object.keys(comandosImplementados) }));
});

app.post('/login', function(req, res) {
    res.cookie('nick', req.body.NomeUsuario);
    res.cookie('canais', [req.body.Canal].toString());
    res.cookie('privmsgs', "");
    res.cookie('servidor', req.body.Servidor);

    res.send(JSON.stringify({ status: 200 }));
});

server.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});