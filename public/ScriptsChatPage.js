$(function() {

    function InvocaModalMensagem(Titulo, Mensagem) {
        $('#ModalMensagemTituloModal').get(0).innerText = Titulo;
        $('#ModalMensagemPrincipal_Body').get(0).innerHTML = "";
        $('#ModalMensagemPrincipal_Body').append('<p>' + Mensagem + '</p>');
        $('#BotaoFecharModalMensagem').show();
        $('#ModalMensagemPrincipal').modal('show');
    }

    $('#BotaoFecharModalMensagem').click(function() {
        $('#ModalMensagemPrincipal').modal('hide');
    });

    $('#principal #EnvioMensagem #BotaoEnviarMensagem').click(
        function() {
            var mensagem = $('#principal #EnvioMensagem #EnviarMensagem').val();

            EnviarMensagemServer("post", "/processar_mensagem", mensagem);
            $('#principal #EnvioMensagem #EnviarMensagem').val('');
        }
    );

    //Tratamento do socket.io
    var iosocket = io.connect();

    iosocket.on('connect', function() {

        iosocket.on('message', function(message) {
            console.log(message)
            if (message.channel != null && message.type == "normal") {

                AtualizaPainelCanal(message);

            } else if ((message.command == "privmsg" && message.nick != null && message.type == "normal") || (message.command == "privmsgSend" && message.type == "normal")) {

                AtualizaConversaPrivada(message);

            } else
                AtualizaPainelPrincipal(message);

        });
        iosocket.on('disconnect', function() {
            //Disconnect();
        });
    });

    function AdicionaMensagemCanal(Nick, Channel, Command, TextoMensagem) {
        if (Nick == null) Nick = '';
        if (Channel == null) Channel = '';
        if (Command == null) Command = '';
        if (TextoMensagem == null) TextoMensagem = '';

        var classe = 'Mensagem';

        if (Nick == Cookies.get('nick')) classe = 'MensagemUsuario'

        $('.Conversa').append('<li class="' + classe + '">' +
            '<p class="Remetente">' +
            Nick +
            '<span class="glyphicon glyphicon-globe"  aria-hidden="true"></span>' + Channel +
            '<span class="glyphicon glyphicon-envelope"  aria-hidden="true"></span>' + Command + '</p>' +
            '<p class="TextoMensagem">' + TextoMensagem + '</p>' +
            '</li>');
    }

    //Adiciona os painéis para os canais que estão no Cookie
    AdicionaPrimeirosCanais();

    //Adiciona os painéis para as conversas privadas que estão no Cookie
    AdicionaPrimeirasConversasPrivadas();

    //Adiciona o nome do usuário
    $('.Usuario').text(Cookies.get('nick'));

    //Adiciona o horário            
    $('#Horario').text(ObtemHorario());

    //Adciona funcão do click no botão do home
    $("#tabPrincipal").click(
        function() {
            $(".CanalAtivo").removeClass("CanalAtivo").addClass("CanalLink");
            $("#tabPrincipal").removeClass("CanalLink").addClass("CanalAtivo");

            $("#tabPrincipal #NovasMensagens").text("");
            $("#tabPrincipal #NovasMensagens").removeClass("label label-warning")
        }
    );

    //Preenche campo de comandos implementados
    ComandosImpelmentados("principal");

    //Atualiza horário a cada 30 segundos
    setInterval(() => {
        time = new Date();
        $('.Horario').text(ObtemHorario());
    }, 30000);

    function ObtemHorario() {
        var time = new Date();
        return time.getHours() + ":" + (time.getMinutes() < 10 ? '0' : '') + time.getMinutes();
    }

    function AtualizaConversaPrivada(message) {
        var privmsgs = Cookies.get("privmsgs");

        var nova = false;
        if (privmsgs == null || privmsgs == undefined) {
            nova = true;
            privmsgs = [];
        } else {
            privmsgs = privmsgs.includes(",") ? privmsgs.split(",") : [privmsgs];
        }

        if (nova || !privmsgs.includes(message.nick)) {
            AdicionaConversaPrivada(message.nick);

            privmsgs.push(message.nick);

            if (privmsgs.length > 1)
                Cookies.set("privmsgs", privmsgs.join(","));
            else
                Cookies.set("privmsgs", privmsgs[0]);
        }

        if (message.command != "privmsgSend") {
            var nickHtml = message.nick != null ? `<span class="glyphicon glyphicon-user"  aria-hidden="true"></span> ` + message.nick : "";
            var classMessage = "";
        } else {
            var nickHtml = `<span class="glyphicon glyphicon-user"  aria-hidden="true"></span> Me`;
            var classMessage = " MensagemUsuario";
        }

        var serverHtml = message.server != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-hdd"  aria-hidden="true"></span> ` + message.server : "";
        var commandHtml = message.command != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-envelope"  aria-hidden="true"></span> ` + message.command : "";
        var conversa = $('#' + message.nick + ' .Conversa');

        AtualizaContadorNovasMensagens(message.nick);
        conversa.append(`<li class="Mensagem` + classMessage + `">
                                    <p class="Remetente">` + nickHtml + serverHtml + commandHtml + `</p>
                                    <p class="TextoMensagem">` + message.text + `</p>
                                </li>`);


    }

    function AtualizaPainelCanal(message) {

        var idCanal = message.channel;
        var canais = Cookies.get('canais');
        canais = canais.includes(",") ? canais.split(",") : [canais];

        if (message.command == "names") {
            AtualizaUsuariosAtivos(idCanal, message);
        }

        if (message.command == "join" && message.channel != null && !canais.includes(message.channel)) {
            AdicionaCanal(message.channel);
            canais.push(message.channel);
            Cookies.set("canais", canais.toString());
        }

        if (message.command == "join" && !message.text.includes(Cookies.get("nick")) && message.text.includes('entrou')) {
            $(message.channel + " #UsuariosAtivos ul").append("<li><p>" + message.text.split(" ")[0] + "</p>")
        }

        if (message.command != "privmsgSend") {
            var nickHtml = message.nick != null ? `<span class="glyphicon glyphicon-user"  aria-hidden="true"></span> ` + message.nick : "";
            var classMessage = "";
        } else {
            var nickHtml = `<span class="glyphicon glyphicon-user"  aria-hidden="true"></span> Me`;
            var classMessage = " MensagemUsuario";
        }

        if (message.command == "part") {
            var partNick = message.nick;

            if (partNick == Cookies.get("nick")) {

                $("#tabPrincipal").click();
                $("#tab" + message.channel.replace("#", "")).remove();
                $(message.channel).remove();
            } else {

                $(message.channel + " #UsuariosAtivos ul li").find("p[text='" + partNick + "']").remove();
            }

            return;
        }


        var channelHtml = message.channel != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-globe"  aria-hidden="true"></span> ` + message.channel : "";
        var serverHtml = message.server != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-hdd"  aria-hidden="true"></span> ` + message.server : "";
        var commandHtml = message.command != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-envelope"  aria-hidden="true"></span> ` + message.command : "";
        var conversa = $(idCanal + ' .Conversa');

        conversa.append(`<li class="Mensagem` + classMessage + `">
                                    <p class="Remetente">` + nickHtml + channelHtml + serverHtml + commandHtml + `</p>
                                    <p class="TextoMensagem">` + message.text + `</p>
                                </li>`);

        AtualizaContadorNovasMensagens(message.channel.replace("#", ""));
    }

    function AtualizaPainelPrincipal(message) {

        var nickHtml = message.nick != null ? `<span class="glyphicon glyphicon-user"  aria-hidden="true"></span> ` + message.nick : "";
        var serverHtml = message.server != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-hdd"  aria-hidden="true"></span> ` + message.server : "";
        var commandHtml = message.command != null ? `&nbsp&nbsp&nbsp&nbsp<span class="glyphicon glyphicon-envelope"  aria-hidden="true"></span> ` + message.command : "";
        var conversa = $('#principal .Conversa');

        AtualizaContadorNovasMensagens("Principal");
        conversa.append(`<li class="Mensagem">
                                    <p class="Remetente">` + nickHtml + serverHtml + commandHtml + `</p>
                                    <p class="TextoMensagem">` + message.text + `</p>
                                </li>`);
    }

    function AtualizaUsuariosAtivos(idCanal, message) {

        var nicks = message.text.includes(",") ? message.text.split(",") : [message.text];

        nicks.forEach(function(nick) {
            $(idCanal + ' #UsuariosAtivos ul').append('<li><p>' + nick + '</p><li>')
        });
    }

    function AtualizaContadorNovasMensagens(idCanal) {

        if (!$("#tab" + idCanal).hasClass("CanalAtivo")) {
            var newMessages = $("#tab" + idCanal + " #NovasMensagens");
            var countNewMessages = newMessages.text() == "" ? 1 : parseInt(newMessages.text()) + 1;
            newMessages.addClass("label label-warning");
            newMessages.text(countNewMessages);
        }
    }

    function AdicionaCanal(idCanal) {

        $('.ListaCanais').append(`
                <button id="tab` + idCanal.replace("#", "") + `" class="CanalLink" data-toggle="tab" href="#` + idCanal + `">` + idCanal + `
                    <span id="NovasMensagens"></span>
                    
                </button>
            `);

        $('.tab-content').append(`
                <div id="` + idCanal.replace("#", "") + `" class="tab-pane fade">
                    <div class="DadosEsquerda">
                        <div id="UsuariosAtivos">
                            <p class="Titulo">Usuários Ativos</p>
                            <ul>
                            </ul>
                        </div>
                        <div id="DadosUsuario">
                            <p class="Usuario"></p>
                            <button class="Configuracoes Botao"><span class="glyphicon glyphicon-cog"  aria-hidden="true"></span></button>
                        </div>
                        <div id="DadosServer">
                            <p class="Horario"></p>
                        </div>
                    </div>
                    <div id="HistoricoChat">
                        <ul class="Conversa">
                        </ul>
                    </div>
                    <div id="EnvioMensagem">
                        <input type="text" id="EnviarMensagem" />
                        <button class="Botao" id="BotaoEnviarMensagem"><span class="glyphicon glyphicon-send"  aria-hidden="true"></span></button>
                    </div>
                </div>
        `);

        //Adiciona o nome do usuário
        $(idCanal + ' .Usuario').text(Cookies.get('nick'));

        //Adiciona o horário            
        $(idCanal + ' .Horario').text(ObtemHorario());

        //Adiciona o click no botão
        $(idCanal + ' #EnvioMensagem #BotaoEnviarMensagem').click(
            function() {
                var mensagem = $(idCanal + ' #EnvioMensagem #EnviarMensagem').val();

                EnviarMensagemServer("post", "/processar_mensagem", mensagem);

                $(idCanal + ' #EnvioMensagem #EnviarMensagem').val('');
            }
        );

        $("#tab" + idCanal.replace("#", "")).click(
            function() {

                $(".CanalAtivo").removeClass("CanalAtivo").addClass("CanalLink");
                $("#tab" + idCanal.replace("#", "")).removeClass("CanalLink").addClass("CanalAtivo");

                $("#tab" + idCanal.replace("#", "") + " #NovasMensagens").text("");
                $("#tab" + idCanal.replace("#", "") + " #NovasMensagens").removeClass("label label-warning")
            }
        );

    }

    function AdicionaPrimeirosCanais() {
        var canais = Cookies.get('canais');
        canais = canais.includes(",") ? canais.split(",") : [canais];

        canais.forEach(function(canal) {
            AdicionaCanal(canal);
        });
    }

    function AdicionaPrimeirasConversasPrivadas() {
        var privmsgs = Cookies.get('privmsgs');
        privmsgs = privmsgs.replace(",", "");

        if (privmsgs != null && privmsgs != undefined && privmsgs != "") {
            privmsgs = privmsgs.includes(",") ? privmsgs.split(",") : [privmsgs];

            privmsgs.forEach(function(conversa) {

                AdicionaConversaPrivada(conversa);
            });
        }

    }

    function ComandosImpelmentados(idPainel) {

        $.ajax({
            type: "get",
            url: "/comandos",
            success: function(data, status) {
                if (status == "success") {
                    var comandosImpelmentados = data;

                    comandosImpelmentados.comandos.forEach(function(comando) {
                        $("#" + idPainel + " .DadosEsquerda #ComandosImplementados ul").append('<p>' + comando + '</p>')
                    });

                } else {
                    alert("erro:" + status);
                }

            },
            contentType: "application/json",
            dataType: "json"
        });



    }

    function EnviarMensagemServer(metodo, rota, mensagem) {
        var msg = '{"timestamp":' + Date.now() + ',' +
            '"nick":"' + Cookies.get("nick") + '",' +
            '"msg":"' + mensagem + '"}';

        var retorno = null;

        $.ajax({
            type: metodo,
            url: rota,
            data: msg,
            success: function(data, status) {
                if (status == "success") {
                    retorno = data;
                } else {
                    alert("erro:" + status);
                }

            },
            contentType: "application/json",
            dataType: "json"
        });


    }

    function AdicionaConversaPrivada(nick) {

        $('.ListaCanais').append(`
                        <button id="tab` + nick + `" class="CanalLink" data-toggle="tab" href="#` + nick + `">` + nick + `
                            <span id="NovasMensagens"></span>                            
                        </button>
                    `);

        $('.tab-content').append(`
                        <div id="` + nick + `" class="tab-pane fade">
                            <div class="DadosEsquerda">
                                <div id="ComandosImplementados">
                                    <p class="Titulo">Comandos</p>
                                    <ul>
                                    </ul>
                                </div>
                                <div id="DadosUsuario">
                                    <p class="Usuario"></p>
                                    <button class="Configuracoes Botao"><span class="glyphicon glyphicon-cog"  aria-hidden="true"></span></button>
                                </div>
                                <div id="DadosServer">
                                    <p class="Horario"></p>
                                </div>
                            </div>
                            <div id="HistoricoChat">
                                <ul class="Conversa">
                                </ul>
                            </div>
                            <div id="EnvioMensagem">
                                <input type="text" id="EnviarMensagem" />
                                <button class="Botao" id="BotaoEnviarMensagem"><span class="glyphicon glyphicon-send"  aria-hidden="true"></span></button>
                            </div>
                        </div>
                    `);

        //Adiciona o nome do usuário
        $("#" + nick + ' .Usuario').text(Cookies.get('nick'));

        //Adiciona o horário            
        $("#" + nick + ' .Horario').text(ObtemHorario());

        //Preenche campo de comandos implementados
        ComandosImpelmentados(nick);

        //Adiciona o click no botão
        $("#" + nick + ' #EnvioMensagem #BotaoEnviarMensagem').click(
            function() {
                var mensagem = "privmsg " + nick + " :" + $("#" + nick + ' #EnvioMensagem #EnviarMensagem').val();

                EnviarMensagemServer("post", "/processar_mensagem", mensagem);

                $("#" + nick + ' #EnvioMensagem #EnviarMensagem').val('');
            }
        );

        $("#tab" + nick).click(
            function() {

                $(".CanalAtivo").removeClass("CanalAtivo").addClass("CanalLink");
                $("#tab" + nick).removeClass("CanalLink").addClass("CanalAtivo");

                $("#tab" + nick + " #NovasMensagens").text("");
                $("#tab" + nick + " #NovasMensagens").removeClass("label label-warning")
            }
        );
    }
});