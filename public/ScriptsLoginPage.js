$(function() {
    $('#LoginEntrar').click(function() {
        RealizarLogin()
    });

    function RealizarLogin() {
        var Servidor = $('#ServidorLogin').get(0).value;
        var Porta = $('#PortaLogin').get(0).value;
        var Canal = $('#CanalLogin').get(0).value;
        var Nome = $('#NomeLogin').get(0).value;

        if (Nome == "") {
            InvocaModalMensagem("Erro ao realizar login", "Nome deve ser informado");
        } else if (Servidor == "") {
            InvocaModalMensagem("Erro ao carregar os servidores", "O servidor deve ser informado");
        } else if (Porta == "") {
            InvocaModalMensagem("Erro ao carregar os servidores", "A porta deve ser informada");
        } else if (Canal == "") {
            InvocaModalMensagem("Erro ao carregar os servidores", "O canal deve ser informado");
        } else {
            var login = {
                "Servidor": Servidor,
                "Canal": Canal,
                "NomeUsuario": Nome,
                "Porta": Porta
            };

            $.ajax({
                type: "post",
                url: "/login",
                data: login,
                success: function(retorno) {
                    InvocaModalMensagem("Aguarde", "Redirecionando para o chat");
                    window.location.href = "http://localhost:3000/";
                },
                error: function(retorno) {
                    InvocaModalMensagem("Erro ao realizar login", retorno.responseText);

                }
            });
        }
    }

    function InvocaModalMensagem(Titulo, Mensagem) {
        $('#ModalMensagemTituloModal').get(0).innerText = Titulo;
        $('#ModalMensagemPrincipal_Body').get(0).innerHTML = "";
        $('#ModalMensagemPrincipal_Body').append('<p>' + Mensagem + '</p>');
        $('#BotaoFecharModalMensagem').show();
        $('#ModalMensagemPrincipal').modal('show');
    }

    $('#BotaoFecharModalMensagem').click(function() {
        $('#ModalMensagemPrincipal').modal('hide');
    });
});