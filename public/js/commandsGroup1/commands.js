//Message Class
const Message = require("../model/message").Message;

exports.privmsg = function(ircClient, webSocket, proxy, args) {

    var returnMessage = new Message(null, null, null, null, null);

    if (args.length >= 3) {
        var msg = args.filter(arg => true);
        msg.splice(0, 2);
        msg = msg.join(' ').replace(":", "").trim();
        ircClient.say(args[1], msg);

        returnMessage.nick = !(args[1].charAt(0) == '#') ? args[1].toLowerCase() : null;
        returnMessage.channel = returnMessage.nick == null ? args[1].toLowerCase() : null;
        returnMessage.text = msg;
        returnMessage.command = "privmsgSend";

    } else {
        returnMessage.channel = args.includes("#") ? args[2].toLowerCase() : null;
        returnMessage.text = "Erro de sintaxe.";
        returnMessage.command = "privmsg";
        returnMessage.type = "error";
    }

    webSocket.emit("message", returnMessage);

}

exports.topic = function(ircClient, webSocket, proxy, args) {

    if (args.length == 2) {
        ircClient.send("topic", args[1]);
    } else {
        var msg = args.filter(arg => true);
        msg.splice(0, 2);
        msg = msg.join(' ').replace(":", "").trim();

        ircClient.send("topic", args[1], msg);
    }

}

exports.names = function(ircClient, webSocket, proxy, args) {

    if (args.length == 2) {
        ircClient.send("names", args[1]);
    } else {
        var returnMessage = new Message(null, null, "Erro de sintaxe.", "names", null);
        webSocket.emit("message", returnMessage);
    }

}

exports.join = function(ircClient, webSocket, proxy, args) {

    if (args.length >= 2) {
        var channels = args[1];

        ircClient.join(channels);

        channels = channels.includes(",") ? channels.split(",") : [channels];

        channels.forEach(channel => {

            ircClient.addListener('message' + channel, function(from, message) {
                var returnMessage = new Message(null, channel, message, "join", null);
                webSocket.emit("message", returnMessage.toJson());
            });

        });

        proxy.irc_client = ircClient;
    } else {
        var returnMessage = new Message(null, null, "Erro de sintaxe.", "join", null);
        webSocket.emit("message", returnMessage);
    }
}