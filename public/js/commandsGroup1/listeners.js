//Message Class
const Message = require("../model/message").Message;

exports.AddlistenersGroup1 = function(ircClient, webSocket) {

    ircClient.addListener('names', function(channel, nicks) {
        var returnMessage = new Message(null, channel, Object.keys(nicks).join(", "), "names", null);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('topic', function(channel, topic, nick, message) {
        var returnMessage = new Message(null, message.channel, topic, "topic", message.server);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('pm', function(nick, text, message) {
        var returnMessage = new Message(nick, null, text, "privmsg", message.server);
        webSocket.emit("message", returnMessage.toJson());
    });


    return ircClient;

}