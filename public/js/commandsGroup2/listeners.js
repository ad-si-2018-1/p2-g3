//Message Class
const Message = require("../model/message").Message;

exports.AddlistenersGroup2 = function(ircClient, webSocket) {

    ircClient.addListener('part', function(channel, nick, reason, message) {
        var msg = message.nick + " deixou o canal.";
        var returnMessage = new Message(message.nick, channel, msg, message.command.toLowerCase(), message.server);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('quit', function(nick, reason, channels, message) {

        var returnMessage = new Message(nick, channels.join(", "), reason, message.command, message.server);
        webSocket.emit("message", returnMessage.toJson());
    });

    return ircClient;

}