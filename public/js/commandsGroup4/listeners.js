//Message Class
const Message = require("../model/message").Message;

exports.AddlistenersGroup4 = function(ircClient, webSocket) {

    ircClient.addListener('ping', function(server) {
        var returnMessage = new Message(null, null, server + " PING", "ping", server);
        webSocket.emit("message", returnMessage.toJson());
    });

    return ircClient;

}