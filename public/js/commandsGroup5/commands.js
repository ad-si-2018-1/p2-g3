//Message Class
const Message = require("../model/message").Message;

exports.version = function(ircClient, webSocket, proxy, args) {
    if (args.length == 1) {
        ircClient.send("version");

    } else if (args.length == 2) {
        ircClient.send("version", args[1]);

    } else {
        var errorMessage = new Message(null, null, "Erro de sintaxe.", "version", null);
        errorMessage.type = "error";
        webSocket.emit("message", errorMessage.toJson());
    }
}

exports.motd = function(ircClient, webSocket, proxy, args) {

    if (args.length == 1) {
        ircClient.send("motd");

    } else {
        var errorMessage = new Message(null, null, "Erro de sintaxe.", "motd", null);
        errorMessage.type = "error";
        webSocket.emit("message", errorMessage.toJson());
    }
}