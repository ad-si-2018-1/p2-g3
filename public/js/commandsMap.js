const commandsGroup1 = require("./commandsGroup1/commands");
const commandsGroup2 = require("./commandsGroup2/commands");
const commandsGroup3 = require("./commandsGroup3/commands");
const commandsGroup4 = require("./commandsGroup4/commands");
const commandsGroup5 = require("./commandsGroup5/commands");


exports.commandsMap = {

    //Commands group 1
    "PRIVMSG": commandsGroup1.privmsg,
    "TOPIC": commandsGroup1.topic,
    "JOIN": commandsGroup1.join,
    "NAMES": commandsGroup1.names,

    //Commands group 2
    "NICK": commandsGroup2.nick,
    "USER": commandsGroup2.user,
    "PART": commandsGroup2.part,
    "QUIT": commandsGroup2.quit,

    //Commands group 3
    "TIME": commandsGroup3.time,
    "WHO": commandsGroup3.who,
    "WHOIS": commandsGroup3.whois,
    "LUSERS": commandsGroup3.lusers,

    //Commands group 4
    "PING": commandsGroup4.ping,

    //Commands group 5
    "VERSION": commandsGroup5.version,
    "MOTD": commandsGroup5.motd

}