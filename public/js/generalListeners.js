//Message Class
const Message = require("./model/message").Message;

//Raw commands to listener
var rawListenerCommands = require('./rawListenerCommands').rawListenerCommands;

exports.AddGeneralListeners = function(ircClient, webSocket, channel) {

    ircClient.addListener('message' + channel, function(from, message) {
        var returnMessage = new Message(from, channel, message, null, null);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('registered', function(message) {
        var returnMessage = new Message(null, null, message.args[1], message.command, message.server);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('nick', function(oldnick, newnick, channels, message) {
        var returnMessage = new Message(null, null, "Nick name adicionado/atualizado com sucesso.", message.command, message.server);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('join', function(channel, nick, message) {
        var returnMessage = new Message(nick, channel, nick + " entrou no canal " + channel + ".", "join", null);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('motd', function(mtdo) {
        var returnMessage = new Message(null, null, mtdo, "motd", null);
        webSocket.emit("message", returnMessage.toJson());
    });

    ircClient.addListener('raw', function(message) {
        var command = message.command.toLowerCase();

        if (Object.keys(rawListenerCommands).includes(command)) {
            rawListenerCommands[command](message, webSocket);
        }
    });

    ircClient.addListener('error', function(message) {

        var returnMessage = new Message(null, null, message.args[1], message.command, message.server);
        returnMessage.type = "error";
        webSocket.emit("message", returnMessage.toJson());
    });


    return ircClient;

}