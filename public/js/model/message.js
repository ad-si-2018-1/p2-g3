exports.Message = class {

    constructor(nick, channel, text, command, server) {

        this.time = Date.now();
        this.nick = nick;
        this.channel = channel;
        this.text = text;
        this.command = command;
        this.server = server;
        this.type = "normal";
    }

    toJson() {
        return {
            time: this.time,
            nick: this.nick,
            channel: this.channel,
            text: this.text,
            command: this.command,
            server: this.server,
            type: this.type
        };
    }
};