//Message Class
const Message = require("./model/message").Message;

var version = function(message, webSocket) {
    var returnMessage = new Message(null, null, message.args[1], "version", message.server)
    webSocket.emit("message", returnMessage.toJson());
}

var rpl_time = function(message, webSocket) {

    var returnMessage = new Message(null, null, message.args[message.args.length - 1], "time", message.server)
    webSocket.emit("message", returnMessage.toJson());
}

var who = function(message, webSocket) {

}

var user = function(message, webSocket) {

}

var rpl_notopic = function(message, webSocket) {

    var returnMessage = new Message(null, message.args[1], "Canal sem tópico definido.", "topic", message.server)
    webSocket.emit("message", returnMessage.toJson());
}

exports.rawListenerCommands = {
    "rpl_version": version,
    "rpl_time": rpl_time,
    "who": who,
    "user": user,
    "rpl_notopic": rpl_notopic
};